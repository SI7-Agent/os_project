ccflags-y := -std=gnu99 -Wno-declaration-after-statement
obj-m := time-tracker.o input-tracker.o mouse_driver.o

KERNEL_DIR := /lib/modules/$(shell uname -r)/build
PWD := $(shell pwd)

default:
	$(MAKE) -C $(KERNEL_DIR) M="$(PWD)" modules

clean:
	rm -rf .tmp_versions
	rm *.symvers *.order *.ko .*.*.* *.mod.c *.o
